# xliff-tool

A small script which helps to translate Foundry VTT modules and systems by being able to use translation software ([CAT] software).

It converts translation entries between [Foundrys JSON] format and [XLIFF] 1.2.

XLIFF stands for "XML Localization Interchange File Format" and is used by many translation tools as interchange format.


## Installation

Install [Python](https://www.python.org/downloads/) 3.3 or higher,
then just download the xliff-tool script.

On Windows you run it by using `cmd` or PowerShell: `python .\xliff-tool`.  
On macOS or Linux you *can* use `./xliff-tool` instead.


## Usage Instructions

### General

You can pass the `-v` flag to make the tool be more *verbose* in
what it does. Pass it multiple times to increase its wordiness. Or in other
words: Without it only **errors** are shown. Passing *one* `-v` displays also
**warnings** and by passing the flag twice you'll get **everything**.

The `-d` (or `--dry-run`) flag can be passed when testing things out: No files will be created or modified.


### Possible Workflow

Your workflow will likely look like this:

First, the XLIFF file can be generated from your previous JSON files. Otherwise, you'd have to transfer everything manually.  
`xliff-tool my-translation.xliff create --source-language en-US --source-json en.json --target-language de-DE --target-json de.json`  
You can leave out the translated (target) JSON if you don't have it yet.

Then just open the generated XLIFF file in your favorite translation software and work with it.
Once you're done you can export the translated JSON file:  
`xliff-tool my-translation.xliff export-to translated.json`  
You can add `-t` (or `--tree`) after the `export-to` command to generate a JSON with nested entries.

If the original (source) JSON file has been updated, the changes can be automatically applied to the XLIFF:  
`xliff-tool my-translation.xliff update-from en.json`  
New entries are automatically created at the appropriate place in the XLIFF, changed source texts get a note containing the old text, deleted entries are shown in the log - deleting can also be turned off completely using the `-k` (or `--keep-nonexisting`) flag.


### Detailed Commandline Arguments

```
usage: xliff-tool [-h] [-v] [-d] xliff command ...

positional arguments:
  xliff          The XLIFF file
  command
    create       Create a new XLIFF file
    update-from  Pull changes from the original JSON file
    export-to    Write translations to a JSON file

optional arguments:
  -h, --help
  -v, --verbose  Explain what the program is doing
  -d, --dry-run  Don't write any files

create command:
usage: xliff-tool xliff create [-h] [-s SOURCE_LANGUAGE] -t TARGET_LANGUAGE [--source-json SOURCE_JSON] [--target-json TARGET_JSON]

optional arguments:
  -h, --help            show this help message and exit
  -s SOURCE_LANGUAGE, --source-language SOURCE_LANGUAGE
  -t TARGET_LANGUAGE, --target-language TARGET_LANGUAGE
  --source-json SOURCE_JSON
                        Original JSON file used to seed the XLIFF
  --target-json TARGET_JSON
                        Translated JSON file used to seed the XLIFF

update-from command:
usage: xliff-tool xliff update-from [-h] [-k] json

positional arguments:
  json                  Use JSON file to update the XLIFF

optional arguments:
  -h, --help            show this help message and exit
  -k, --keep-nonexisting
                        Don't remove XLIFF entries if they aren't existing in the source (anymore)

export-to command:
usage: xliff-tool xliff export-to [-h] [-t] json

positional arguments:
  json        Target JSON file

optional arguments:
  -h, --help  show this help message and exit
  -t, --tree  Export as tree structure (Dots are used to split and group keys)
```


## FAQ

### Why is it using the old XLIFF 1.2 format?

To my knowledge there is sadly no free software which understands 2.x.

And having freely available software is a strict requirement, considering that most are translating voluntarily.


### Are there any recommended translation tools?

Here are three free ones:

- [Poedit](https://poedit.net/)
- [Virtaal](https://virtaal.translatehouse.org/)
- [Lokalize](https://userbase.kde.org/Lokalize)


## License and copyright

The xliff-tool is licensed under the MIT license, which can be found in the script header.



[CAT]: https://en.wikipedia.org/wiki/Computer-assisted_translation
[XLIFF]: https://en.wikipedia.org/wiki/XLIFF
[Foundrys JSON]: https://foundryvtt.com/article/localization/#structure
